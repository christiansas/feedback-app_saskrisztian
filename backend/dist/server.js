"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const mongoose_1 = __importDefault(require("mongoose"));
const dotenv_1 = __importDefault(require("dotenv"));
const feedback_1 = require("./feedback");
const express_graphql_1 = require("express-graphql");
const schema_1 = require("./feedback/graphQL/schema");
const mutation_1 = require("./feedback/graphQL/mutation");
const query_1 = require("./feedback/graphQL/query");
dotenv_1.default.config();
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use((0, cors_1.default)());
app.use("/api/feedback", feedback_1.router);
const resolvers = {
    ...query_1.query,
    ...mutation_1.mutation,
};
app.use("/graphql", (0, express_graphql_1.graphqlHTTP)((request, response, graphQLParams) => ({
    schema: schema_1.schema,
    rootValue: resolvers,
    graphiql: true,
    context: {
        request,
        response,
    },
})));
app.get("/", (req, res) => {
    res.send("Welcome to my backend...");
});
const port = process.env.PORT || 5000;
app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log(`Server running on port: ${port}...`);
});
const atlasUri = process.env.ATLAS_URI;
mongoose_1.default
    .connect(atlasUri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
})
    // tslint:disable-next-line:no-console
    .then(() => console.log("MongoDB connection established..."))
    // tslint:disable-next-line:no-console
    .catch((error) => console.error("MongoDB connection failed:", error.message));
