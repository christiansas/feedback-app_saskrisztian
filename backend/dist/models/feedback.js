"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const feedbackSchema = new mongoose_1.default.Schema({
    rating: { type: Number, required: true },
    text: { type: String, required: true, minlength: 10, maxlength: 300 },
}, {
    timestamps: true,
});
const FeedbackModel = mongoose_1.default.model("Feedback", feedbackSchema);
exports.default = FeedbackModel;
