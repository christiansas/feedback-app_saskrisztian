"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mutation = void 0;
const dbInteraction_1 = require("./dbInteraction");
exports.mutation = {
    addFeedback: async ({ rating, text }) => {
        try {
            const feedback = await (0, dbInteraction_1.addFeedback)(rating, text);
            return {
                data: feedback,
                ok: true,
                error: "",
            };
        }
        catch (error) {
            return {
                data: null,
                ok: false,
                error,
            };
        }
    },
    deleteFeedback: async ({ id }) => {
        // const book = books.find(book => book.id === id)
        // books = books.filter(book => book.id !== id)
        try {
            const feedback = await (0, dbInteraction_1.deleteFeedback)(id);
            if (!feedback) {
                return {
                    data: null,
                    ok: false,
                    error: "Feedback not found",
                };
            }
            return {
                data: feedback,
                ok: true,
                error: "",
            };
        }
        catch (error) {
            return {
                data: null,
                ok: false,
                error,
            };
        }
    },
};
