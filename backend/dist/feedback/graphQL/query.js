"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.query = void 0;
const dbInteraction_1 = require("./dbInteraction");
exports.query = {
    feedbacks: async () => {
        return await (0, dbInteraction_1.getFeedbacks)();
    },
    feedback: async ({ id }) => {
        return await (0, dbInteraction_1.getFeedbackById)(id);
    },
    test: () => {
        return "Test";
    },
};
