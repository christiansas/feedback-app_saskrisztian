"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.schema = void 0;
const graphql_1 = require("graphql");
exports.schema = (0, graphql_1.buildSchema)(`
  type Query {
    feedbacks: [Feedback]
    feedback(id: ID!): Feedback
    test: String
  }
  type Mutation {
    addFeedback(rating: Int!, text: String!): FeedbackResponse
    deleteFeedback(id: ID!): FeedbackResponse
  }
  type Feedback {
    _id: ID!
    rating: Int!
    text: String!
  }
  type Feedbacks {
    feedbacks: [Feedback]
  }
  type FeedbackResponse {
    data: Feedback
    error: String
    ok: Boolean
  }
`);
