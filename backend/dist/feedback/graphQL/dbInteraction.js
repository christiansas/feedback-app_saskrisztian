"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addFeedback = exports.deleteFeedback = exports.getFeedbackById = exports.getFeedbacks = void 0;
const mongo_1 = require("../mongo");
const getFeedbacks = async () => {
    return await mongo_1.DbModel.find().sort([["_id", -1]]);
};
exports.getFeedbacks = getFeedbacks;
const getFeedbackById = async (_id) => {
    return await mongo_1.DbModel.findById(_id);
};
exports.getFeedbackById = getFeedbackById;
const deleteFeedback = async (_id) => {
    return await mongo_1.DbModel.findByIdAndDelete(_id);
};
exports.deleteFeedback = deleteFeedback;
const addFeedback = async (rating, text) => {
    const feedback = new mongo_1.DbModel({ rating, text });
    await feedback.save();
};
exports.addFeedback = addFeedback;
