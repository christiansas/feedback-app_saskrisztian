"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.router = void 0;
const mongo_1 = require("./mongo");
const express_1 = __importDefault(require("express"));
const joi_1 = __importDefault(require("joi"));
exports.router = express_1.default.Router();
/**
 * @swagger
 * /feedback:
 *   get:
 *     summary: Retrieve a list of feedbacks
 *     description: Retrieve a list of feedbacks from MongoDB. Can be used to populate a list of fake users when prototyping or testing an API.
 */
exports.router.get("/", async (req, res) => {
    try {
        const feedbacks = await mongo_1.DbModel.find().sort([["_id", -1]]);
        const results = feedbacks.map((feedback) => ({
            _id: feedback._id,
            rating: feedback.rating,
            text: feedback.text,
        }));
        res.send(results);
    }
    catch (error) {
        // tslint:disable-next-line:no-console
        console.log(error.message);
        res.status(500).send("Error: " + error.message);
    }
});
exports.router.post("/", async (req, res) => {
    try {
        const schema = joi_1.default.object({
            rating: joi_1.default.number().min(1).max(5).required(),
            text: joi_1.default.string().min(10).max(300).required(),
        });
        const { error } = schema.validate(req.body);
        if (error)
            return res.status(400).send(error.details[0].message);
        const { rating, text } = req.body;
        const feedback = new mongo_1.DbModel({ rating, text });
        await feedback.save();
        const result = {
            _id: feedback._id,
            rating: feedback.rating,
            text: feedback.text,
        };
        res.send(result);
    }
    catch (error) {
        // tslint:disable-next-line:no-console
        console.log(error.message);
        res.status(500).send(error.message);
    }
});
exports.router.delete("/:id", async (req, res) => {
    try {
        const deletedFeedback = await mongo_1.DbModel.findByIdAndDelete(req.params.id);
        if (!deletedFeedback)
            return res.status(404).send("Feedback not found...");
        const result = {
            _id: deletedFeedback._id,
            rating: deletedFeedback.rating,
            text: deletedFeedback.text,
        };
        res.send(result);
    }
    catch (error) {
        return res.status(404).send("Feedback not found...");
    }
});
exports.router.get("/:id", async (req, res) => {
    try {
        const feedback = await mongo_1.DbModel.findById(req.params.id);
        if (!feedback)
            return res.status(404).send("Feedback not found...");
        const result = {
            _id: feedback._id,
            rating: feedback.rating,
            text: feedback.text,
        };
        res.send(result);
    }
    catch (error) {
        return res.status(404).send("Feedback not found...");
    }
});
