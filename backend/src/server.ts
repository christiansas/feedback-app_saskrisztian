import cors from "cors";
import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import { ConnectOptions } from "mongoose";
import { graphqlHTTP } from "express-graphql";
import { schema } from "./feedback/graphQL/schema";
import { mutation } from "./feedback/graphQL/mutation";
import { query } from "./feedback/graphQL/query";

dotenv.config();

const app = express();

app.use(express.json());
app.use(cors());

const resolvers = {
  ...query,
  ...mutation,
};

app.use(
  "/graphql",
  graphqlHTTP((request, response, graphQLParams) => ({
    schema,
    rootValue: resolvers,
    graphiql: true,
    context: {
      request,
      response,
    },
  }))
);

app.get("/", (req, res) => {
  res.send("Welcome to my backend...");
});

const port = process.env.PORT || 5000;
app.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`Server running on port: ${port}...`);
});

const atlasUri: string = process.env.ATLAS_URI!;
mongoose
  .connect(atlasUri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
  } as ConnectOptions)
  // tslint:disable-next-line:no-console
  .then(() => console.log("MongoDB connection established..."))
  // tslint:disable-next-line:no-console
  .catch((error) => console.error("MongoDB connection failed:", error.message));
