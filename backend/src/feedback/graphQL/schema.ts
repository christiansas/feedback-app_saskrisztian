import { buildSchema } from "graphql";

export const schema = buildSchema(`
  type Query {
    feedbacks: [Feedback!]!
    feedback(id: ID!): Feedback
    test: String
  }
  type Mutation {
    addFeedback(rating: Int!, text: String!): Feedback!
    deleteFeedback(id: ID!): Boolean!
  }
  type Feedback {
    _id: ID!
    rating: Int!
    text: String!
  }
`);

export interface Feedback {
  _id: string;
  rating: number;
  text: string;
}

export type Id = string;
