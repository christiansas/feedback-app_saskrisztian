import { deleteFeedback, addFeedback } from "../db/db";

export const mutation = {
  addFeedback: async ({ rating, text }: { rating: number; text: string }) => {
    return await addFeedback(rating, text);
  },

  deleteFeedback: async ({ id }: { id: string }) => {
    await deleteFeedback(id);
    return true;
  },
};
