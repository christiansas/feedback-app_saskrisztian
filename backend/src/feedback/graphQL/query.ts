import { getFeedbacks, getFeedbackById } from "../db/db";
import { Feedback, Id } from "./schema";

export const query = {
  feedbacks: async (): Promise<Feedback[]> => {
    return await getFeedbacks();
  },

  feedback: async ({ id }: { id: Id }): Promise<Feedback | null> => {
    return await getFeedbackById(id);
  },
  test: () => {
    return "Test";
  },
};
