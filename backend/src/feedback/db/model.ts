export interface Feedback {
  _id: string;
  rating: number;
  text: string;
}
