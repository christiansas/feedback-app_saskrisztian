import { DbModel } from "./mongo";

export const getFeedbacks = async () => {
  return await DbModel.find().sort([["_id", -1]]);
};

export const getFeedbackById = async (_id: string) => {
  return await DbModel.findById(_id);
};

export const deleteFeedback = async (_id: string) => {
  return await DbModel.findByIdAndDelete(_id);
};

export const addFeedback = async (rating: number, text: string) => {
  const feedback = new DbModel({ rating, text });
  await feedback.save();
};
