import mongoose from "mongoose";
import { Feedback } from "./model";

const feedbackSchema = new mongoose.Schema<Feedback>(
  {
    rating: { type: Number, required: true },
    text: { type: String, required: true, minlength: 10, maxlength: 300 },
  },
  {
    timestamps: true,
  }
);

export const DbModel = mongoose.model("Feedback", feedbackSchema);
