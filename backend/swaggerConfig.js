module.exports = {
  info: {
    title: "Express API for my Feedback-App",
    version: "1.0.0",
    description:
      "This is a REST API application made with Express. It retrieves data from a cloud-hosted MongoDB.",
    license: {
      name: "Licensed Under MIT",
      url: "https://spdx.org/licenses/MIT.html",
    },
  },
  apis: ["./src/routes/feedbacks.ts"],
};
