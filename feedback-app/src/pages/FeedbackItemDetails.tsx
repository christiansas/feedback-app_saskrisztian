import React from "react";
import CardForms from "../components/shared/CardForms";
import Button from "@mui/material/Button";
import { Link, useParams } from "react-router-dom";
import { useGetFeedbackByIdQuery } from "../Redux/services/api";

function FeedbackItemDetails(): JSX.Element {
  const { id } = useParams();
  console.log(id);

  const {
    data: feedback,
    error,
    isLoading,
    isSuccess,
  } = useGetFeedbackByIdQuery(id!);

  return (
    <div>
      {isLoading && <p>...Loading Feedback</p>}
      {error && <p>Something went wrong</p>}
      {isSuccess && (
        <CardForms>
          <div>
            <p>
              <b>Feedback Id: </b>
              {feedback!._id}
            </p>
            <p>
              <b>Feedback Text: </b>
              {feedback!.text}
            </p>
            <p>
              <b>Feedback Rating: </b>
              {feedback!.rating}
            </p>
          </div>
          <Link to="/">
            <Button variant="contained" color="primary">
              Back
            </Button>
          </Link>
        </CardForms>
      )}
    </div>
  );
}

export default FeedbackItemDetails;
