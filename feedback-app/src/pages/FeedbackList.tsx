import React from "react";
import FeedbackCard from "../components/FeedbackItem";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import { Container, ListGroup } from "react-bootstrap";
import { useGetFeedbacksQuery } from "../Redux/services/api";
import { useDeleteFeedbackMutation } from "../Redux/services/api";

function FeedbackList(): JSX.Element {
  const {
    data: feedbacks,
    error,
    isLoading,
    isSuccess,
  } = useGetFeedbacksQuery();

  //Send data to DB via Redux RTK
  const [deleteFeedback] = useDeleteFeedbackMutation();

  const deleteFeedbackHandler = async (_id: string) => {
    await deleteFeedback(_id);
  };

  return (
    <Container>
      <ListGroup style={{ marginBottom: "1rem" }}>
        <div>
          {isLoading && <h2>...Loading Feedbacks</h2>}
          {error && <h2>Something went wrong</h2>}
        </div>
        <TransitionGroup>
          {isSuccess &&
            feedbacks!.map(({ _id, rating, text }) => (
              <CSSTransition
                key={_id}
                timeout={500}
                classNames="feebacklist"
                appear="true"
              >
                <ListGroup.Item>
                  <FeedbackCard
                    key={_id}
                    _id={_id}
                    rating={rating}
                    text={text}
                    handleDelete={deleteFeedbackHandler}
                  />
                </ListGroup.Item>
              </CSSTransition>
            ))}
        </TransitionGroup>
      </ListGroup>
    </Container>
  );
}

export default FeedbackList;
