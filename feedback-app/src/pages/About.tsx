import React from "react";
import { Link } from "react-router-dom";
import Card from "../components/shared/CardForms";

function About() {
  return (
    <Card>
      <div className="about">
        <Link to="/">Back to Landing Page</Link>
      </div>
    </Card>
  );
}

export default About;
