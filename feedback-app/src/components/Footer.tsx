import React from "react";
import HelpCenterIcon from "@mui/icons-material/HelpCenter";
import { Link } from "react-router-dom";

function Footer() {
  return (
    <footer>
      <div className="Footer">
        <Link to="/about">
          <HelpCenterIcon />
        </Link>
      </div>
    </footer>
  );
}

export default Footer;
