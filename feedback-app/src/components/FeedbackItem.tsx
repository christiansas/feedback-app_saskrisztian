import React from "react";
import Card from "./shared/CardItems";
import { FaTimes } from "react-icons/fa";
import { Link } from "react-router-dom";

interface FeedbackItemProps {
  _id: string;
  rating: number;
  text: string;
  handleDelete(_id: any): void;
}

function FeedbackCard(props: FeedbackItemProps): JSX.Element {
  const { _id, rating, text, handleDelete } = props;

  const deleteFeedback = () => {
    handleDelete(_id);
  };

  return (
    <Card>
      <Link to={`/feedback/${props._id}`}>
        <div className="click-area"></div>
      </Link>
      <div className="num-display">{rating}</div>
      <div className="text-display">{text}</div>
      <button className="close">
        <FaTimes color="purple" onClick={deleteFeedback} />
      </button>
    </Card>
  );
}

export default FeedbackCard;
