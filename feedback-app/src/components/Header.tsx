import React from "react";
import HeaderDarkModeSwitch from "./HeaderDarkModeSwitch";

interface HeaderProps {
  title: string;
}

function Header(props: HeaderProps): JSX.Element {
  return (
    <header className="Header">
      <h1>{props.title}</h1>
      <HeaderDarkModeSwitch />
    </header>
  );
}

export default Header;
