import * as React from "react";
import Box from "@mui/material/Box";
import { StyledRating } from "./shared/styles/FeedbackRatingStyle";

interface FeedbackRatingProps {
  setRating: (value: number) => void;
}

export default function FeedbackRating(props: FeedbackRatingProps) {
  const [value, setValue] = React.useState<number | null>(2);
  const { setRating } = props;

  const handleNewValue = (newValue: number | null) => {
    setRating(newValue!);
  };

  return (
    <div className="rating-stars">
      <Box
        sx={{
          "& > legend": { mt: 2 },
        }}
      >
        <StyledRating
          precision={0.5}
          name="customized-color"
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
            handleNewValue(newValue);
          }}
        />
      </Box>
    </div>
  );
}
