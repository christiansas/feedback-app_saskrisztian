import React from "react";
import FeedbackList from "../pages/FeedbackList";
import FeedbackStats from "./FeedbackStats";
import FeedbackForm from "./FeeedbackForm";

export const RootPage = (): JSX.Element => {
  return (
    <>
      <FeedbackForm />
      <FeedbackStats />
      <FeedbackList />
    </>
  );
};
