import React from "react";
import { useGetFeedbacksQuery } from "../Redux/services/api";

function FeedbackStats(): JSX.Element {
  const {
    data: feedbacks,
    error,
    isLoading,
    isSuccess,
  } = useGetFeedbacksQuery();

  let ratings: number[];
  let average: number = 0;

  if (isSuccess == true) {
    ratings = feedbacks!.map((f) => f.rating);
    average = roundTo2Digits(calcAverage(ratings));
  }

  return (
    <div>
      {isLoading && <p>...Loading Data</p>}
      {error && <p>Something went wrong</p>}
      {isSuccess && (
        <div className="feedback-stats">
          <h4>{feedbacks!.length} Reviews</h4>
          <h4>Average Rating: {isNaN(average) ? 0 : average}</h4>
        </div>
      )}
    </div>
  );
}

export default FeedbackStats;

const calcAverage = (array: number[]): number =>
  array.reduce((acc, cur) => acc + cur, 0) / array.length;

const roundTo2Digits = (number: number): number =>
  Math.round(number * 100) / 100;
