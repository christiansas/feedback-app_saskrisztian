import React, { PropsWithChildren } from "react";
import { useAppSelector } from "../../Redux/hooks";
import { darkModeEnabled } from "../../Redux/features/darkMode/darkModeSlice";
import { CardProps } from "./cardModel";

function CardItems(props: PropsWithChildren<CardProps>) {
  //Redux handlers
  const darkModeState = useAppSelector(darkModeEnabled);

  return (
    <div className={`itemsC ${darkModeState === true ? "darkMode" : null}`}>
      {props.children}
    </div>
  );
}

export default CardItems;
