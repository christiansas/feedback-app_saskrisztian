import Rating from "@mui/material/Rating";
import { styled } from "@mui/material/styles";

export const StyledRating = styled(Rating)({
  "& .MuiRating-iconFilled": {
    color: "#ff6a95",
  },
  "& .MuiRating-iconHover": {
    color: "#ff6a95;",
  },
});
