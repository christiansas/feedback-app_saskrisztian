import CardForms from "./shared/CardForms";
import { useState } from "react";
import FeedbackRating from "./FeedbackRating";
import * as React from "react";
import FeedbackFormSButton from "./FeedbackFormSButton";
import { useAddFeedbackMutation } from "../Redux/services/api";

function FeeedbackForm(): JSX.Element {
  const [text, setText] = useState<string>("");
  const [rating, setRating] = useState<number>(2);
  const [send, setSend] = useState<boolean>(true);

  //Send data to DB via Redux RTK
  const [addFeedback] = useAddFeedbackMutation();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setText(e.target.value);
    //Only enable Button when the user wrote at least 10 characters
    e.target.value.length >= 10 ? setSend(false) : setSend(true);
  };

  const addFeedbackHandler = async () => {
    const feedback = {
      text: text,
      rating: rating,
    };

    //Add new Feedback to DB
    await addFeedback(feedback);

    //Reset input-fields
    setText("");
    setSend(true);
  };

  return (
    <CardForms>
      <div className="input-area">
        <h2>How would you rate your service with us?</h2>
        <FeedbackRating setRating={setRating} />
        <div className="input-group">
          <input
            onChange={handleChange}
            type="text"
            placeholder="Please enter at least 10 characters!"
            value={text}
          />
          <FeedbackFormSButton
            disabled={send}
            handleClick={addFeedbackHandler}
          />
        </div>
      </div>
    </CardForms>
  );
}

export default FeeedbackForm;
