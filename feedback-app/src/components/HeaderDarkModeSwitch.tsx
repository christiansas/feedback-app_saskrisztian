import * as React from "react";
import { useAppDispatch } from "../Redux/hooks";
import { setDarkMode } from "../Redux/features/darkMode/darkModeSlice";
import { GreenSwitch } from "./shared/styles/HeaderDarkModeSwitchStyle";

function HeaderDarkModeSwitch() {
  //Redux handlers
  const dispatch = useAppDispatch();

  const handleChangeNew = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setDarkMode(event.target.checked));
  };

  return (
    <div className="switch-area">
      <GreenSwitch onChange={handleChangeNew} />
      <label>Dark Mode</label>
    </div>
  );
}

export default HeaderDarkModeSwitch;
