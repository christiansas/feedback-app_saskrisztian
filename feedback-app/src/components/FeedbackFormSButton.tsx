import React from "react";
import Button from "@mui/material/Button";

interface FeedbackFormSButtonProps {
  handleClick(): void;
  disabled: boolean;
}

function FeedbackFormSButton(props: FeedbackFormSButtonProps) {
  const { handleClick, disabled } = props;

  return (
    <div>
      <Button disabled={disabled} variant="contained" onClick={handleClick}>
        Send
      </Button>
    </div>
  );
}

export default FeedbackFormSButton;
