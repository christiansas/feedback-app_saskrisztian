export interface DarkModeState {
  darkModeToggle: boolean;
}
