import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import type { RootState } from "../../store";
import { DarkModeState } from "./darkModeModel";

const initialState: DarkModeState = {
  darkModeToggle: false,
};

export const darkModeSlice = createSlice({
  name: "darkMode",
  initialState,
  reducers: {
    setDarkMode: (state, action: PayloadAction<boolean>) => {
      state.darkModeToggle = action.payload;
    },
  },
});

export const darkModeEnabled = (state: RootState) =>
  state.darkMode.darkModeToggle;

export const { setDarkMode } = darkModeSlice.actions;

export default darkModeSlice.reducer;
