import { createApi } from "@reduxjs/toolkit/query/react";
import { request, gql, ClientError } from "graphql-request";

export interface Feedback {
  _id: string;
  rating: number;
  text: string;
}

type AddFeedbackParam = Omit<Feedback, "_id">;

const graphqlBaseQuery =
  ({ baseUrl }: { baseUrl: string }) =>
  async ({ body }: { body: string }) => {
    try {
      const result = await request(baseUrl, body);
      return { data: result };
    } catch (error) {
      if (error instanceof ClientError) {
        return { error: { status: error.response.status, data: error } };
      }
      return { error: { status: 500, data: error } };
    }
  };

export const feedbackApi = createApi({
  baseQuery: graphqlBaseQuery({
    baseUrl: "http://localhost:5000/graphql/",
  }),
  tagTypes: ["Feedback"],
  endpoints: (builder) => ({
    getFeedbacks: builder.query<Feedback[], void>({
      query: () => ({
        body: gql`
          query {
            feedbacks {
              _id
              rating
              text
            }
          }
        `,
      }),
      providesTags: ["Feedback"],
      transformResponse: (response) => response.feedbacks,
    }),
    getFeedbackById: builder.query<Feedback, string>({
      query: (id) => ({
        body: gql`
        query {
          feedback(id: "${id}") {
            _id
            rating
            text
          }
        }
      `,
      }),
      providesTags: ["Feedback"],
      transformResponse: (response) => response.feedback,
    }),
    addFeedback: builder.mutation<void, AddFeedbackParam>({
      query: (feedback) => ({
        body: gql`
        mutation {
          addFeedback(rating: ${feedback.rating}, text: "${feedback.text}"){
            ok
            error
          }
        }
      `,
      }),
      invalidatesTags: ["Feedback"],
      transformResponse: (response) => response.addFeedback,
    }),
    deleteFeedback: builder.mutation<void, string>({
      query: (id) => ({
        body: gql`
        mutation {
          deleteFeedback(id: "${id}"){
            ok
            error
          }
        }
      `,
      }),
      invalidatesTags: ["Feedback"],
      transformResponse: (response) => response.deleteFeedback,
    }),
  }),
});

export const {
  useGetFeedbacksQuery,
  useGetFeedbackByIdQuery,
  useAddFeedbackMutation,
  useDeleteFeedbackMutation,
} = feedbackApi;
