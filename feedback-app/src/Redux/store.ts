import { configureStore } from "@reduxjs/toolkit";
import darkModeReducer from "./features/darkMode/darkModeSlice";
import { combineReducers } from "redux";
import { feedbackApi } from "./services/api";
import { setupListeners } from "@reduxjs/toolkit/query";

const reducers = combineReducers({
  darkMode: darkModeReducer,
  [feedbackApi.reducerPath]: feedbackApi.reducer,
});

const store = configureStore({
  reducer: reducers,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(feedbackApi.middleware),
});

setupListeners(store.dispatch);

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
