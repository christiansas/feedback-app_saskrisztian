import React, { createContext, useContext } from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import "./App.css";
import About from "./pages/About";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import FeedbackItemDetails from "./pages/FeedbackItemDetails";
import { RootPage } from "./components/RootPage";
import { Provider } from "react-redux";
import store from "./Redux/store";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/integration/react";

let persistor = persistStore(store);

function App() {
  return (
    <Provider store={store}>
      {/* <PersistGate persistor={persistor} loading={null}> */}
      <Router>
        <div className="App">
          <Header title="Feedback UI" />
          <div className="container">
            <Routes>
              <Route path="/" element={<RootPage />}></Route>
              <Route path="/about" element={<About />} />
              <Route
                path={`/feedback/:id`}
                element={<FeedbackItemDetails />}
              ></Route>
            </Routes>
          </div>
          <Footer />
        </div>
      </Router>
      {/* </PersistGate> */}
    </Provider>
  );
}

export default App;
